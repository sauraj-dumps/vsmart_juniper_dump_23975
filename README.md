## juniper_open-user 10 QKQ1.200603.001 V441A_OPN_U_A3_201022 release-keys
- Manufacturer: vsmart
- Platform: trinket
- Codename: juniper
- Brand: vsmart
- Flavor: juniper_open-user
- Release Version: 10
- Id: QKQ1.200603.001
- Incremental: V441A_OPN_U_A3_201022
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: vsmart/juniper_open/juniper:10/QKQ1.200603.001/V441A_OPN_U_A3_201022:user/release-keys
- OTA version: 
- Branch: juniper_open-user-10-QKQ1.200603.001-V441A_OPN_U_A3_201022-release-keys
- Repo: vsmart_juniper_dump_23975


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
